import { Link } from 'react-router-dom';
import './index.css'

function MainPage() {

  return (
    <body>
      <main role="main">

        <div style={{ height: "350px" }} class="jumbotron container-fluid bg-image bg-dark">
          <div class="container">
            <h1 class="display-3">CarCar</h1>
            <p>The premiere solution for automobile dealership management</p>
          </div>
        </div>
        <div class="container mt-4">
          <div class="row">
            <div class="col-md-4">
              <h2>Inventory</h2>
              <p>Ready to expand your inventory? <br></br> Maximize your efficiency by effortlessly keeping track of the vital details for each vehicle, including make, model, year, VIN, and more. Get started now by creating an inventory entry with the click of a button. </p>
              <p><Link class="btn btn-secondary" to="/automobiles/create" role="button">Add Inventory &raquo;</Link></p>
            </div>
            <div class="col-md-4">
              <h2>Sales</h2>
              <p> Creating a new sale is just a click away. Our streamlined interface guides you through the process, ensuring accuracy and efficiency. Easily enter customer details, select the desired vehicle from your inventory, and customize the terms of the sale.  </p>
              <p><Link class="btn btn-secondary" to="/sales/create" role="button">Record a New Sale &raquo;</Link></p>
            </div>
            <div class="col-md-4">
              <h2>Services</h2>
              <p>Initiating a new appointment is a breeze with our user-friendly interface. Our intuitive system effortlessly navigates you through the process, allowing you to input customer information, choose the preferred service, and assign a skilled technician.</p>
              <p><Link class="btn btn-secondary" to="#" role="button">Add a Service Appointment &raquo;</Link></p>
            </div>
          </div>

          <hr></hr>

        </div>

      </main>
    </body>
  );
}

export default MainPage;
