import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    // <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
    //   <div className="container-fluid">
    //     <NavLink className="navbar-brand" to="/">CarCar</NavLink>
    //     <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    //       <span className="navbar-toggler-icon"></span>
    //     </button>
    //     <div className="collapse navbar-collapse" id="navbarSupportedContent">
    //       <ul className="nav navbar-nav me-auto mb-2 mb-lg-0">
    //         <NavLink className="nav-link" to="/technicians/create"> Add a Technician</NavLink>
    //         <NavLink className="nav-link" to="/technicians"> Technicians</NavLink>
    //         <NavLink className="nav-link" to="/appointments/create"> Create a Service Appointment</NavLink>
    //         <NavLink className="nav-link" to="/appointments"> Service Appointments</NavLink>
    //       </ul>
    //     </div>
    //   </div>
    // </nav >
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mx-5mb-2 mb-lg-0">
            <li class="nav-item">
              <NavLink className="navbar-brand" to="/">CarCar</NavLink>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </a>
              <ul class="dropdown-menu">
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to="/manufacturers/create">Create a Manufacturer</NavLink></li>
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to="/manufacturers">Manufacturers</NavLink></li>
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to="/models/create">Create a Model</NavLink></li>
                <li class="dropdown-item text-dark"  ><NavLink style={{ color: 'black', textDecoration: "none" }} to="/models">Models</NavLink></li>
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to='/automobiles/create'>Add Automobile to Inventory</NavLink></li>
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to='/automobiles'>Inventory</NavLink></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </a>
              <ul class="dropdown-menu">
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to="/sales/create">Record a New Sale</NavLink></li>
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to="/sales/history">Sales History</NavLink></li>
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to="/customers/create">Add a Customer</NavLink></li>
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to="/customers">Customers</NavLink></li>
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to='/salespeople/create'>Add a Salesperson</NavLink></li>
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to='/salespeople'>Salespeople</NavLink></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Services
              </a>
              <ul class="dropdown-menu">
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to="/appointments/create">Create a Service Appointment</NavLink></li>
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to="/appointments">Service Appointments</NavLink></li>
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to="/service_history">Service History</NavLink></li>
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to="/technicians/create">Add a Technician</NavLink></li>
                <li class="dropdown-item text-dark"><NavLink style={{ color: 'black', textDecoration: "none" }} to='/technicians'>Technicians</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
